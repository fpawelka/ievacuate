sap.ui.define(["sap/ui/core/mvc/Controller",
    "sap/m/MessageBox",
    "sap/ui/model/json/JSONModel",
    "./utilities",
    "sap/ui/core/routing/History"
    ], function(BaseController, MessageBox, JSONModel, Utilities, History) {
    "use strict";

    return BaseController.extend("generated.app.controller.Page1", {
    	openDialog: function(){
		      this.oDialog = new sap.ui.xmlfragment("generated.app.view.Page2", this);
		      this.oDialog.open();
	    },
	    closeDialog: function(message){
	    	if (message !== undefined) {
	    		console.log(message);
	    	}
	    	this.oDialog.close();
	    	this._data.buses = 2;
	    	this._data.firefighters = 0;
	    	sap.m.MessageToast.show("Evacuation process started");
	    	setTimeout(function () {
	    		sap.m.MessageToast.show("Notification (SMS) sent to population");
	    	}, 3000);
	    	setTimeout(function () {
	    		sap.m.MessageToast.show("Notification sent to rescue team 2");
	    	}, 6000);
	    	document.getElementById('__item4-intro').innerText = 'Evacuation in progress';
	    	window.evacuationCircles[1].setOptions( {strokeColor: 'orange', fillColor: 'orange'});
	    },
	
    handleRouteMatched: function (oEvent) {
            		
		var oParams = {}; 
		
		if (oEvent.mParameters.data.context) { 
		    this.sContext = oEvent.mParameters.data.context;
		    var oPath; 
		    if (this.sContext) { 
		        oPath = {path: "/" + this.sContext, parameters: oParams}; 
		        this.getView().bindObject(oPath);
		    } 
		}
		
		
		
		
		
     },
     
     
		_data : {
			"dtValue" : new Date(),
			"buses": 3,
			"firefighters": 10
//			"dtValue" : "08:15:32"
		},
onInit: function () {
         
        this.mBindingOptions = {};
        this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        this.oRouter.getTarget("Page1").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
        
			var oModel = new JSONModel(this._data);
			this.getView().setModel(oModel);
			var that = this;
			setInterval(function () {
				that._data.dtValue = new Date();
				var oModel = new JSONModel(that._data);
				that.getView().setModel(oModel);
			}, 1000);
			
			setTimeout(function () {
				document.getElementById('__item4').addEventListener('click', function () {
			      that.oDialog = new sap.ui.xmlfragment("generated.app.view.Page2", that);
			      that.oDialog.open();
				});
			}, 1000);
        

        }
});

}, /* bExport= */true);