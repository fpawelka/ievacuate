# iEvacuate

As more and more data of all kind (like demographic, climate, geographic data) 
is available, there needs to be a way to use it in emergency situations for good. 
But just presenting the raw data streams willbe overwhelming for decision makers. 
So we need a system that simplifies andaggreates the existing data and present it 
in a clear and simple way. Only ifthe decision makers get the right information 
at the right time they can makeinformed decisions on how to distribute resources 
in emergency situations.

Meet our decision support system iEvacuate from the SAP Next-Gen Bootcamp in 
Divnom�rskoye, 1st and 2nd of september 2017.

- Hans Friedrich Pawelka, WWU M�nster
- Maria Gruzdeva, ITMO University
- Inessa Popova, VSU
- Dmitry Voronov, VSU
- Valentina Okhrimenko, BelSU
- Igor Ryazanov, NRU HSE

## Getting started

1. Go to SAP Web IDE and create an account
2. On account start page click on "Clone from Git Repository"
3. Enter `https://bitbucket.org/fpawelka/ievacuate.git`
4. Right click on created folder `ievacuate` -> `Run` -> `Run as` -> `Web Application`
5. Choose `testFLPServiceMockServer.html`
6. Enjoy the prototype

## Important

Please bear in mind, that this is **not** an example of good programming style 
but just a (really basic) proof of concept. All lot of functionality is justed 
mocked and not really implemented. There was neither follewed the SAP UI5 structure 
nor basic programming principles like DRY (dont repeat yourself) or loose coupling. 

## Presentation

Our presentation can be found in this repository as pdf (`./iEvacuate.pdf`) or as 
[pptx on dropbox](https://www.dropbox.com/s/o8p1rooip3a8hke/iEvacuate.pptx?dl=0)
